////  SwiftUI2_StickyFooterViewApp.swift
//  SwiftUI2_StickyFooterView
//
//  Created on 05/02/2021.
//  
//

import SwiftUI

@main
struct SwiftUI2_StickyFooterViewApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
