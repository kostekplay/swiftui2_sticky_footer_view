////  ContentView.swift
//  SwiftUI2_StickyFooterView
//
//  Created on 05/02/2021.
//  
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
